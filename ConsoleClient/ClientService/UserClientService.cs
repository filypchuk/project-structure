﻿using Common.DTO.DtoForSevenLinqMethods;
using Common.DTO.User;
using ConsoleClient.HttpRequests;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleClient.ClientService
{
    public class UserClientService
    {
        private readonly HttpRequest _request;
        public UserClientService()
        {
            _request = new HttpRequest();
        }
        public IEnumerable<UserDto> GetAll()
        {
            return _request.GetAll<UserDto>("users");
        }
        public UserDto GetById(int id)
        {
            return _request.GetById<UserDto>("users", id);
        }
        public UserDto Create(UserCreateDto createDto)
        {
            var json = JsonConvert.SerializeObject(createDto);
            return _request.Create<UserDto>("users", json);
        }
        public UserDto Update(UserDto updateDto)
        {
            var json = JsonConvert.SerializeObject(updateDto);
            return _request.Update<UserDto>("users", json);
        }
        public void Delete(int id)
        {
            _request.Delete("users", id);
        }
        public IEnumerable<UserByAlphabetAndTasksDto> UserByAlphabet()
        {
            return _request.GetAll<UserByAlphabetAndTasksDto>("users/user-by-alphabet");
        }
        public UsersProjectTaskDto UserLastProject(int userId)
        {
            return _request.GetById<UsersProjectTaskDto>("users/user-last-project", userId);
        }
    }
}
