﻿namespace Common.Enum
{
    public enum TaskStates
    {
        Created = 0,
        Started,
        Finished,
        Canceled
    }
}
