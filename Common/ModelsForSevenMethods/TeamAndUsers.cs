﻿using DataAccessLayer.Entities;
using System.Collections.Generic;

namespace Common.ModelsForSevenMethods
{
    public sealed class TeamAndUsers
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<User> Users { get; set; }
    }

}
