﻿using System;

namespace Common.DTO.Project
{
    public sealed class ProjectCreateDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }
        public int AuthorId { get; set; }
        public int TeamId { get; set; }
    }
}
