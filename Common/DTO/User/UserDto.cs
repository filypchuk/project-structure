﻿using System;

namespace Common.DTO.User
{
    public sealed class UserDto
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime RegisteredAt { get; set; }
        public int? TeamId { get; set; }
        public override string ToString()
        {
            return $"Id -- {Id}\n FirstName -- {FirstName}\n LastName -- {LastName}\n " +
                $"Email -- {Email}\n Birthday -- {Birthday}\n " +
                $"RegisteredAt -- {RegisteredAt}\n TeamId -- {TeamId}\n";
        }
    }
}
