﻿using System;

namespace Common.DTO.Team
{
    public sealed class TeamDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
        public override string ToString()
        {
            return $"Id -- {Id}\n Name -- {Name}\n CreatedAt -- {CreatedAt}\n ";
        }
    }
}
