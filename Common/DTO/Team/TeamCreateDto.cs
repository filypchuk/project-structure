﻿namespace Common.DTO.Team
{
    public sealed class TeamCreateDto
    {
        public string Name { get; set; }
    }
}
