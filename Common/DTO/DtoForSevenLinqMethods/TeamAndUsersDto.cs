﻿using Common.DTO.User;
using System.Collections.Generic;

namespace Common.DTO.DtoForSevenLinqMethods
{
    public sealed class TeamAndUsersDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<UserDto> Users { get; set; }
    }
}
