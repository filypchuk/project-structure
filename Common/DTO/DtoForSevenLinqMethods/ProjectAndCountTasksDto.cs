﻿using Common.DTO.Project;

namespace Common.DTO.DtoForSevenLinqMethods
{
    public sealed class ProjectAndCountTasksDto
    {
        public ProjectDto Key { get; set; }
        public int Value { get; set; }
    }
}
