﻿using System;

namespace Common.DTO.Task
{
    public sealed class TaskCreateDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime FinishedAt { get; set; }
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
    }
}
