﻿using DataAccessLayer.Entities;
using System.Collections.Generic;
using System.Linq;

namespace DataAccessLayer.Repository
{
    public class Repository
    {
        private readonly ReadDataFromJson readData;
        public IEnumerable<Project> Projects { get; private set; }
        public IEnumerable<TaskEntity> Tasks { get; private set; }
        public IEnumerable<Team> Teams { get; private set; }
        public IEnumerable<User> Users { get; private set; }
        public Repository()
        {
            readData = new ReadDataFromJson();

            Projects = readData.ProjectsData();
            Tasks = readData.TasksData();
            Teams = readData.TeamsData();
            Users = readData.UsersData();

            JoinAllEntity();
        }
        public void SaveProjects(List<Project> list)
        {
            Projects = list;
        }
        public void SaveTasks(List<TaskEntity> list)
        {
            Tasks = list;
        }
        public void SaveTeams(List<Team> list)
        {
            Teams = list;
        }
        public void SaveUsers(List<User> list)
        {
            Users = list;
        }
        private void JoinAllEntity()
        {
            Projects = from project in Projects
                       join user in Users on project.AuthorId equals user.Id
                       join team in Teams on project.TeamId equals team.Id
                       join task in Tasks on project.Id equals task.ProjectId into tasks
                       select new Project
                       {
                           Id = project.Id,
                           Name = project.Name,
                           Description = project.Description,
                           CreatedAt = project.CreatedAt,
                           Deadline = project.Deadline,
                           AuthorId = project.AuthorId,
                           User = user,
                           TeamId = project.TeamId,
                           Team = team,
                           Tasks = from t in tasks.DefaultIfEmpty()
                                   where t != null
                                   join us in Users on t.PerformerId equals us.Id
                                   select new TaskEntity
                                   {
                                       Id = t.Id,
                                       Name = t.Name,
                                       Description = t.Description,
                                       CreatedAt = t.CreatedAt,
                                       FinishedAt = t.FinishedAt,
                                       State = t.State,
                                       ProjectId = t.ProjectId,
                                       Project = project,
                                       PerformerId = t.PerformerId,
                                       User = us,
                                   }
                       };
        }
    }
}
