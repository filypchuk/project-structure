﻿using DataAccessLayer.Entities;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;

namespace DataAccessLayer.Repository
{
    public class ReadDataFromJson
    {
        public List<Project> ProjectsData()
        {
            return ReadAndDeserializeData<Project>("Projects.json");
        }
        public List<TaskEntity> TasksData()
        {
            return ReadAndDeserializeData<TaskEntity>("Tasks.json");
        }
        public List<Team> TeamsData()
        {
            return ReadAndDeserializeData<Team>("Teams.json");
        }
        public List<User> UsersData()
        {
            return ReadAndDeserializeData<User>("Users.json");
        }
        private List<T> ReadAndDeserializeData<T>(string fileName)
        {
            string str = "";
            using (StreamReader sr = new StreamReader($"../DataAccessLayer/JsonData/{fileName}"))
            {
                str = sr.ReadToEnd();
            }
            return JsonConvert.DeserializeObject<List<T>>(str);
        }
    }
}
