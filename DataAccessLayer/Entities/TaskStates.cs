﻿namespace DataAccessLayer.Entities
{
    public enum TaskStates
    {
        Created = 0,
        Started,
        Finished,
        Canceled
    }
}
