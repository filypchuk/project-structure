﻿using System;
using System.Collections.Generic;

namespace DataAccessLayer.Entities
{
    public class Team
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
        public IEnumerable<User> Users { get; set; }
    }
}
