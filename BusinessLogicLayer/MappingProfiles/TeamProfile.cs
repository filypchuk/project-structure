﻿using AutoMapper;
using Common.DTO.DtoForSevenLinqMethods;
using Common.DTO.Team;
using Common.ModelsForSevenMethods;
using DataAccessLayer.Entities;

namespace BusinessLogicLayer.MappingProfiles
{
    public class TeamProfile : Profile
    {
        public TeamProfile()
        {
            CreateMap<Team, TeamDto>();
            CreateMap<TeamDto, Team>();
            CreateMap<TeamCreateDto, Team>();
            CreateMap<TeamAndUsers, TeamAndUsersDto>();
        }
    }
}
