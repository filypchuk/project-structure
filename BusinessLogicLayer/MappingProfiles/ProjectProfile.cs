﻿using AutoMapper;
using Common.DTO.DtoForSevenLinqMethods;
using Common.DTO.Project;
using Common.ModelsForSevenMethods;
using DataAccessLayer.Entities;

namespace BusinessLogicLayer.MappingProfiles
{
    public class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<Project, ProjectDto>();
            CreateMap<ProjectDto, Project>();
            CreateMap<ProjectCreateDto, Project>(); 
            CreateMap<ProjectAndTwoTasks, ProjectAndTwoTasksDto>();
            CreateMap<ProjectAndCountTasks, ProjectAndCountTasksDto>();
        }
    }
}
