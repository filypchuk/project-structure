﻿using AutoMapper;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.Services.Abstract;
using Common.DTO.DtoForSevenLinqMethods;
using Common.DTO.Task;
using DataAccessLayer.Entities;
using DataAccessLayer.Repository;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BusinessLogicLayer.Services
{
    public class TaskService : BaseService, ITaskService
    {
        public TaskService(Repository repository, IMapper mapper) : base(repository, mapper) { }
        public IEnumerable<TaskDto> GetAll()
        {
            return _mapper.Map<IEnumerable<TaskDto>>(_repository.Tasks);
        }

        public TaskDto GetById(int id)
        {
            return _mapper.Map<TaskDto>(_repository.Tasks.FirstOrDefault(entity => entity.Id == id));
        }
        public TaskDto Create(TaskCreateDto dto)
        {
            int nextId = _repository.Tasks.OrderBy(entity => entity.Id).LastOrDefault().Id + 1;
            var newTask = _mapper.Map<TaskEntity>(dto);
            newTask.Id = nextId;
            newTask.CreatedAt = DateTime.Now;
            newTask.State = TaskStates.Created;
            var list = _repository.Tasks.ToList();
            list.Add(newTask);
            _repository.SaveTasks(list);

            return _mapper.Map<TaskDto>(newTask);
        }

        public TaskDto Update(TaskDto dto)
        {
            var list = _repository.Tasks.ToList();
            int index = list.FindIndex(entity => entity.Id == dto.Id);
            if (index != -1)
            {
                var entityUpdate = _mapper.Map<TaskEntity>(dto);
                list[index] = entityUpdate;
                _repository.SaveTasks(list);
                return _mapper.Map<TaskDto>(entityUpdate);
            }
            return null;
        }

        public void Delete(int id)
        {
            var find = _repository.Tasks.FirstOrDefault(entity => entity.Id == id);
            if (find != null)
            {
                var list = _repository.Tasks.ToList();
                list.Remove(find);
                _repository.SaveTasks(list);
            }
        }
        public IEnumerable<TaskDto> TasksByUser(int userId)
        {
            var tasks = _repository.Tasks
                .Where(task => task.PerformerId == userId && task.Name.Length < 45);

            return _mapper.Map<IEnumerable<TaskDto>>(tasks);
        }
        public IEnumerable<TasksFinishedByUserDto> TasksFinishedByUser(int userId)
        {
            return _repository.Tasks
                .Where(task => task.PerformerId == userId && task.FinishedAt.Year == DateTime.Now.Year)
                .Select(t => new TasksFinishedByUserDto {Id = t.Id, Name = t.Name });
        }
    }
}
