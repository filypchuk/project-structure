﻿using AutoMapper;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.Services.Abstract;
using Common.DTO.DtoForSevenLinqMethods;
using Common.DTO.User;
using Common.ModelsForSevenMethods;
using DataAccessLayer.Entities;
using DataAccessLayer.Repository;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BusinessLogicLayer.Services
{
    public class UserService : BaseService, IUserService
    {
        public UserService(Repository repository, IMapper mapper) : base(repository, mapper) { }
        public IEnumerable<UserDto> GetAll()
        {
            return _mapper.Map<IEnumerable<UserDto>>(_repository.Users);
        }

        public UserDto GetById(int id)
        {
            return _mapper.Map<UserDto>(_repository.Users.FirstOrDefault(entity => entity.Id == id));
        }
        public UserDto Create(UserCreateDto dto)
        {
            int nextId = _repository.Users.OrderBy(entity => entity.Id).LastOrDefault().Id + 1;
            var newUser = _mapper.Map<User>(dto);
            newUser.Id = nextId;
            newUser.RegisteredAt = DateTime.Now;
            var list = _repository.Users.ToList();
            list.Add(newUser);
            _repository.SaveUsers(list);
            return _mapper.Map<UserDto>(newUser);
        }

        public UserDto Update(UserDto dto)
        {
            var list = _repository.Users.ToList();
            int index = list.FindIndex(entity => entity.Id == dto.Id);
            if (index != -1)
            {
                var entityUpdate = _mapper.Map<User>(dto);
                list[index] = entityUpdate;
                _repository.SaveUsers(list);
                return _mapper.Map<UserDto>(entityUpdate);
            }
            return null;
        }
        public void Delete(int id)
        {
            var find = _repository.Users.FirstOrDefault(entity => entity.Id == id);
            if (find != null)
            {
                var list = _repository.Users.ToList();
                list.Remove(find);
                _repository.SaveUsers(list);
            }
        }
        public IEnumerable<UserByAlphabetAndTasksDto> UserByAlphabetAndTasks()
        {
            var userByAlphabet = _repository.Users
                .GroupJoin(_repository.Tasks,
                u => u.Id,
                t => t.PerformerId,
                (user, tasks) =>
                  new UserByAlphabetAndTasks
                  {
                      User = user,
                      Tasks =tasks.OrderByDescending(t => t.Name.Length)
                  })
                  .OrderBy(u => u.User.FirstName);
            return _mapper.Map<IEnumerable<UserByAlphabetAndTasksDto>>(userByAlphabet);
        }
        public UsersProjectTaskDto UsersLastProjectAndTasks(int userId)
        {
            var usersProjectTask = _repository.Users.Where(x => x.Id == userId)
                .GroupJoin(_repository.Projects,
                u => u.Id,
                p => p.AuthorId,
                (user, projects) => new UsersProjectTask
                {
                    User = user,
                    LastProject = projects.OrderBy(p => p.CreatedAt).LastOrDefault(),
                    CountTasksByLastProject = projects.OrderBy(p => p.CreatedAt).LastOrDefault()?.Tasks.Count() ?? 0,
                    CountStartCancelTasks = projects.GroupJoin(_repository.Tasks,
                                            p => p.AuthorId,
                                            t => t.PerformerId,
                                            (p, tasks) => tasks
                                            .Where(t => t.State == TaskStates.Canceled || t.State == TaskStates.Started))?.Count() ?? 0,
                    TheLongestByTimeTask = _repository.Tasks.Where(t => t.PerformerId == userId).OrderBy(x => x.FinishedAt - x.CreatedAt).LastOrDefault()

                }).LastOrDefault();
            return _mapper.Map<UsersProjectTaskDto>(usersProjectTask);
        }

    }
}
