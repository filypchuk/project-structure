﻿using AutoMapper;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.Services.Abstract;
using Common.DTO.DtoForSevenLinqMethods;
using Common.DTO.Project;
using Common.DTO.Task;
using Common.ModelsForSevenMethods;
using DataAccessLayer.Entities;
using DataAccessLayer.Repository;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BusinessLogicLayer.Services
{
    public class ProjectService : BaseService, IProjectService
    {
        public ProjectService(Repository repository, IMapper mapper) : base(repository, mapper) { }
        public IEnumerable<ProjectDto> GetAll()
        {
            return _mapper.Map<IEnumerable<ProjectDto>>(_repository.Projects);
        }
        public ProjectDto GetById(int id)
        {
            return _mapper.Map<ProjectDto>(_repository.Projects.FirstOrDefault(entity => entity.Id == id));
        }
        public ProjectDto Create(ProjectCreateDto dto)
        {
            int nextId = _repository.Projects.OrderBy(entity => entity.Id).LastOrDefault().Id + 1;
            var newProject = _mapper.Map<Project>(dto);
            newProject.Id = nextId;
            newProject.CreatedAt = DateTime.Now;
            var list = _repository.Projects.ToList();
            list.Add(newProject);
            _repository.SaveProjects(list);
            return _mapper.Map<ProjectDto>(newProject);
        }
        public ProjectDto Update(ProjectDto dto)
        {
            var list = _repository.Projects.ToList();           
            int index = list.FindIndex(entity => entity.Id == dto.Id);
            if(index !=-1)
            { 
                var entityUpdate = _mapper.Map<Project>(dto);
                list[index] = entityUpdate;
                _repository.SaveProjects(list);
                return _mapper.Map<ProjectDto>(entityUpdate);
            }
            return null;
        }
        public void Delete(int id)
        {
            var find = _repository.Projects.FirstOrDefault(entity => entity.Id == id);
            if (find != null)
            {
                var list = _repository.Projects.ToList();
                list.Remove(find);
                _repository.SaveProjects(list);
            }
        }
        public IEnumerable<ProjectAndCountTasksDto> TasksInProjectByUser(int userId)
        {
           var projectsAndCount = _repository.Projects
                    .Where(p => p.AuthorId == userId)
                    .Select(x=> 
                    new ProjectAndCountTasks
                    { 
                        Key = x, 
                        Value = x.Tasks?.Count() ?? 0
                    });
            return _mapper.Map<IEnumerable<ProjectAndCountTasksDto>>(projectsAndCount);
        }
        public IEnumerable<ProjectAndTwoTasksDto> AllProjectsWithTheLongestTaskAndTheShortest()
        {
            var projectsAndTasks = _repository.Projects
                .GroupJoin(_repository.Users,
                    p => p.TeamId,
                    u => u.TeamId,
                    (project, teamUsers) => new ProjectAndTwoTasks
                    {
                        Project = project,
                        TheLongestTask = project.Tasks?.OrderBy(task => task.Description.Length).LastOrDefault(),
                        TheShortestTask = project.Tasks?.OrderBy(task => task.Name.Length).FirstOrDefault(),
                        CountUsersInTeam = teamUsers
                                  .Where(u => project.Description.Length > 20 || (project.Tasks?.Count() ?? 0) < 3)?
                                  .Count() ?? 0
                    });
            return _mapper.Map<IEnumerable<ProjectAndTwoTasksDto>>(projectsAndTasks);
        }
    }
}
