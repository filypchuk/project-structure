﻿using AutoMapper;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.MappingProfiles;
using BusinessLogicLayer.Services.Abstract;
using Common.DTO.DtoForSevenLinqMethods;
using Common.DTO.Team;
using Common.ModelsForSevenMethods;
using DataAccessLayer.Entities;
using DataAccessLayer.Repository;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BusinessLogicLayer.Services
{
    public class TeamService : BaseService, ITeamService
    {
        public TeamService(Repository repository, IMapper mapper) : base(repository, mapper) { }
        public IEnumerable<TeamDto> GetAll()
        {
            return _mapper.Map<IEnumerable<TeamDto>>(_repository.Teams);
        }

        public TeamDto GetById(int id)
        {
            return _mapper.Map<TeamDto>(_repository.Teams.FirstOrDefault(entity => entity.Id == id));
        }
        public TeamDto Create(TeamCreateDto dto)
        {
            int nextId = _repository.Teams.OrderBy(entity => entity.Id).LastOrDefault().Id + 1;
            var newTeam = _mapper.Map<Team>(dto);
            newTeam.Id = nextId;
            newTeam.CreatedAt = DateTime.Now;
            var list = _repository.Teams.ToList();
            list.Add(newTeam);
            _repository.SaveTeams(list);
            return _mapper.Map<TeamDto>(newTeam);
        }
        public TeamDto Update(TeamDto dto)
        {
            var list = _repository.Teams.ToList();
            int index = list.FindIndex(entity => entity.Id == dto.Id);
            if (index != -1)
            {
                var entityUpdate = _mapper.Map<Team>(dto);
                list[index] = entityUpdate;
                _repository.SaveTeams(list);
                return _mapper.Map<TeamDto>(entityUpdate);
            }
            return null;
        }
        public void Delete(int id)
        {
            var find = _repository.Teams.FirstOrDefault(entity => entity.Id == id);
            if (find != null)
            {
                var list = _repository.Teams.ToList();
                list.Remove(find);
                _repository.SaveTeams(list);
            }
        }
        public IEnumerable<TeamAndUsersDto> TeamAndUsersOlderTenYears()
        {
            var teamAndUsers = _repository.Teams
                .GroupJoin(_repository.Users,
                        t => t.Id,
                        u => u.TeamId,
                        (team, users) =>
                        new TeamAndUsers
                        {
                            Id = team.Id,
                            Name = team.Name,
                            Users = users.Where(u => u.Birthday.Year <= DateTime.Now.Year - 10)
                                    .OrderByDescending(u => u.RegisteredAt)
                        })
                .Where(team => team.Users.Any(u => u.Birthday.Year <= DateTime.Now.Year - 10));
            return _mapper.Map<IEnumerable<TeamAndUsersDto>>(teamAndUsers);
        }
    }
}
