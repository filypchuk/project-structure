﻿using AutoMapper;
using DataAccessLayer.Repository;

namespace BusinessLogicLayer.Services.Abstract
{
    public abstract class BaseService
    {
        private protected readonly Repository _repository;
        private protected readonly IMapper _mapper;

        public BaseService(Repository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }
    }
}
