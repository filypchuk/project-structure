﻿using Common.DTO.DtoForSevenLinqMethods;
using Common.DTO.Team;
using System.Collections.Generic;

namespace BusinessLogicLayer.Interfaces
{
    public interface ITeamService
    {
        public IEnumerable<TeamDto> GetAll();
        public TeamDto GetById(int id);
        public TeamDto Create(TeamCreateDto dto);
        public TeamDto Update(TeamDto dto);
        public void Delete(int id);
        public IEnumerable<TeamAndUsersDto> TeamAndUsersOlderTenYears();
    }
}